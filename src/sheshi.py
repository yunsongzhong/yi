import random
import time


class SheShi():
    def __init__(self):
        super().__init__()

    @staticmethod
    def qi(shi=[]):
        random.seed(time.time())

        gua = []
        if shi == []:
            for i in range(6):
                y = SheShi.yao()
                gua.append(y)
        else:
            for i in range(6):
                y = SheShi.yao(shi[i])
                gua.append(y)
        return gua

    @staticmethod
    def yao(shi=[]):
        origin_remain = 49
        if shi == []:
            first = SheShi.bian(origin_remain)
        else:
            first = SheShi.bian(origin_remain, shi[0])
        first_bian = first[0]
        first_remain = first[1]

        if shi == []:
            second = SheShi.bian(first_remain)
        else:
            second = SheShi.bian(first_remain, shi[0])
        second_bian = second[0]
        second_remain = second[1]

        if shi == []:
            third = SheShi.bian(second_remain)
        else:
            third = SheShi.bian(second_remain, shi[0])
        third_bian = third[0]
        third_remain = third[1]

        count = 0
        if first_bian > 7:
            count += 1
        if second_bian > 7:
            count += 1
        if third_bian > 7:
            count += 1

        yao_result = ""
        if count == 0:
            yao_result = "老阳"
        if count == 1:
            yao_result = "少阴"
        if count == 2:
            yao_result = "少阳"
        if count == 3:
            yao_result = "老阴"
        return yao_result

    @staticmethod
    def bian(begin, seperator=0):
        if seperator == 0:
            left = random.randint(2, begin - 2)
        else:
            left = seperator
        right = begin - 1 - left
        right_little = left % 4
        if right_little == 0:
            right_little = 4
        left_big = right % 4
        if left_big == 0:
            left_big = 4
        bian = 1 + left_big + right_little
        remain = begin - bian
        return [bian, remain]


def main():
    gua = SheShi.qi()
    print(gua)


if __name__ == '__main__':
    main()
