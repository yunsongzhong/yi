import sheshi
import jinqian
import pandas


class Duan():
    def __init__(self):
        super().__init__()

    @staticmethod
    def read_gua(xiang):
        guaxiang = dict()
        g = pandas.read_csv("data/gua.csv")
        for item in g.values:
            guaxiang[item[0]] = item[1]
        return guaxiang[xiang]

    @staticmethod
    def bian_gua(bengua, bianshu):
        biangua = ""
        for i in range(6):
            if bianshu[i] == "老":
                if bengua[i] == "阴":
                    biangua += "阳"
                if bengua[i] == "阳":
                    biangua += "阴"
            if bianshu[i] == "少":
                biangua += bengua[i]
        return biangua

    @staticmethod
    def duan(gua):
        bengua = "".join([x[1] for x in gua])
        benguaming = Duan.read_gua(bengua)

        bianshu = "".join([x[0] for x in gua])
        biangua = Duan.bian_gua(bengua, bianshu)
        bianguaming = Duan.read_gua(biangua)

        is_zhu_ci = False

        count_bian = len([x for x in bianshu if x == "老"])
        yaoci = []
        if count_bian == 0:
            yaoci = [benguaming]
        if count_bian == 1:
            yc = Duan.zhao_bian_yao(bengua, bianshu)
            yaoci = [benguaming + yc[0]]
        if count_bian == 2:
            is_zhu_ci = True
            yc = Duan.zhao_bian_yao(bengua, bianshu)
            yaoci = [benguaming + yc[1], benguaming + yc[0]]
        if count_bian == 3:
            yaoci = [benguaming, bianguaming]
        if count_bian == 4:
            is_zhu_ci = True
            yc = Duan.zhao_ben_yao(bengua, bianshu)
            yaoci = [benguaming + yc[0], benguaming + yc[1]]
        if count_bian == 5:
            yc = Duan.zhao_ben_yao(biangua, bianshu)
            yaoci = [bianguaming + yc[0]]
        if count_bian == 6:
            if benguaming == "乾":
                yaoci = ["乾用九"]
            elif benguaming == "坤":
                yaoci = ["坤用六"]
            else:
                yaoci = [bianguaming]

        guaci = []
        guaci.append("本卦：" + benguaming)
        guaci.append("变卦：" + bianguaming)
        guaci.append("卜辞：")
        for item in yaoci:
            guaci.append(item + "，" + Duan.read_yao(item))
        if is_zhu_ci:
            guaci.insert(-1, "\n")
        guaci = "\n".join(guaci)
        return guaci

    @staticmethod
    def zhao_bian_yao(guaming, bianshu):
        names = []
        sits = [i + 1 for i, x in enumerate(bianshu) if x == "老"]
        for item in sits:
            if item == 1:
                if guaming[item - 1] == "阳":
                    names.append("初九")
                else:
                    names.append("初六")
            if item == 2:
                if guaming[item - 1] == "阳":
                    names.append("九二")
                else:
                    names.append("六二")
            if item == 3:
                if guaming[item - 1] == "阳":
                    names.append("九三")
                else:
                    names.append("六三")
            if item == 4:
                if guaming[item - 1] == "阳":
                    names.append("九四")
                else:
                    names.append("六四")
            if item == 5:
                if guaming[item - 1] == "阳":
                    names.append("九五")
                else:
                    names.append("六五")
            if item == 6:
                if guaming[item - 1] == "阳":
                    names.append("上九")
                else:
                    names.append("上六")
        return names

    @staticmethod
    def zhao_ben_yao(guaming, bianshu):
        names = []
        sits = [i + 1 for i, x in enumerate(bianshu) if x == "少"]
        for item in sits:
            if item == 1:
                if guaming[item - 1] == "阳":
                    names.append("初九")
                else:
                    names.append("初六")
            if item == 2:
                if guaming[item - 1] == "阳":
                    names.append("九二")
                else:
                    names.append("六二")
            if item == 3:
                if guaming[item - 1] == "阳":
                    names.append("九三")
                else:
                    names.append("六三")
            if item == 4:
                if guaming[item - 1] == "阳":
                    names.append("九四")
                else:
                    names.append("六四")
            if item == 5:
                if guaming[item - 1] == "阳":
                    names.append("九五")
                else:
                    names.append("六五")
            if item == 6:
                if guaming[item - 1] == "阳":
                    names.append("上九")
                else:
                    names.append("上六")
        return names

    @staticmethod
    def read_yao(yaoming):
        yaoci = dict()
        g = pandas.read_csv("data/ci.csv")
        for item in g.values:
            yaoci[item[0]] = item[1]
        return yaoci[yaoming]


def main():
    gua = jinqian.JinQian.qi()
    gua = sheshi.SheShi.qi()
    guaci = Duan.duan(gua)
    print(guaci)


if __name__ == '__main__':
    main()
