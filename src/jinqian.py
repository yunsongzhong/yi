import random
import time


class JinQian():
    def __init__(self):
        super().__init__()

    @staticmethod
    def qi(jinqian=[]):
        random.seed(time.time())

        gua = []
        if jinqian == []:
            for i in range(6):
                gua.append(JinQian.yao())
        else:
            z = 0
            for i in range(6):
                gua.append(JinQian.yao(
                    jinqian[z], jinqian[z + 1], jinqian[z + 2]))
                z += 3
        return gua

    @staticmethod
    def yao(first=-1, second=-1, third=-1):
        count = 0

        if first == -1:
            first = random.randint(0, 1)
        if second == -1:
            second = random.randint(0, 1)
        if third == -1:
            third = random.randint(0, 1)

        if first == 1:
            count += 1
        if second == 1:
            count += 1
        if third == 1:
            count += 1

        yao_result = ""
        if count == 0:
            yao_result = "老阳"
        if count == 1:
            yao_result = "少阴"
        if count == 2:
            yao_result = "少阳"
        if count == 3:
            yao_result = "老阴"
        return yao_result


def main():
    gua = JinQian.qi()
    print(gua)


if __name__ == '__main__':
    main()
