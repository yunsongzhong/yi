# Yi

#### Description
{**Yi**} is a Yi-related program set.

#### Software Architecture

This software is a collection of several independent functions.

#### Installation

No need to install, just copy and use it!

#### Instructions

1.  data folder includes data tables, you can read it to learn some knowledge.
2.  src folder includes background services.

#### Contribution

This project is developed by [zhongyunsong](https://gitee.com/yunsongzhong).